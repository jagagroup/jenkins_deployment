# Use the official Ubuntu 22.04 LTS image as the base image
FROM ubuntu:22.04

# Set environment variables for non-interactive installation
ENV DEBIAN_FRONTEND=noninteractive

# Install necessary dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        openjdk-11-jdk \
        wget \
        gnupg \
    && rm -rf /var/lib/apt/lists/*


# Create the Jenkins directory
RUN mkdir -p /usr/share/jenkins

# Download and add the Jenkins GPG key to the keyring
RUN gpg --batch --keyserver keyserver.ubuntu.com --recv-keys 5BA31D57EF5975CA \
    && gpg --export --armor 5BA31D57EF5975CA | gpg --dearmor -o /usr/share/keyrings/jenkins-archive-keyring.gpg

# Add Jenkins repository to sources list
RUN echo "deb [signed-by=/usr/share/keyrings/jenkins-archive-keyring.gpg] https://pkg.jenkins.io/debian-stable binary/" > /etc/apt/sources.list.d/jenkins.list

# Import the Jenkins GPG key to apt
RUN gpg --no-default-keyring --keyring /usr/share/keyrings/jenkins-archive-keyring.gpg --import /usr/share/keyrings/jenkins-archive-keyring.gpg

# Make sure the keyring file has the correct permissions
RUN chmod 644 /usr/share/keyrings/jenkins-archive-keyring.gpg

# Update package information
RUN apt-get update


# Install Jenkins
RUN apt-get install -y --no-install-recommends jenkins

# Download Jenkins.war file
RUN wget -q -O /usr/share/jenkins/jenkins.war https://get.jenkins.io/war/2.434/jenkins.war

# Verify the downloaded Jenkins.war file
#RUN echo "3d9a3d0d93fcb5cbcc2ca2124d40be1a  /usr/share/jenkins/jenkins.war" | md5sum -c -

# Expose the default Jenkins port
EXPOSE 8080

# Optionally expose the agent port (if you plan to connect agents)
EXPOSE 50000

# Set up Jenkins home directory as a volume
VOLUME ["/tmp/jenkins_volume"]

# Start Jenkins as the entry point
CMD ["java", "-jar", "/usr/share/jenkins/jenkins.war", "--httpPort=8080"]
